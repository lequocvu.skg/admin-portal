require('dotenv').config({ path: '.env.development.local' })
const config = require('./nuxt.config.js')
const nuxtConfig = { ...config }

module.exports = nuxtConfig
