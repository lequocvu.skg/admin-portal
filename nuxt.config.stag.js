require('dotenv').config({ path: '.env.staging' })
const config = require('./nuxt.config.js')
const nuxtConfig = { ...config }

module.exports = nuxtConfig
