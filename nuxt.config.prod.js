require('dotenv').config({ path: '.env.production' })
const config = require('./nuxt.config.js')
const nuxtConfig = { ...config }

module.exports = nuxtConfig
