
export default {
  mode: 'spa',
  env: {
    dev: (process.env.NODE_ENV !== 'production')
  },
  srcDir: 'client/',
  head: {
    title: 'SuperEHR | Admin Portal',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'SuperEHR | Admin Portal' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico?v3' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap'
      }
    ]
  },
  loading: '@/components/tools/Loading.vue',
  loadingIndicator: {
    name: 'cube-grid',
    color: '#ff7875',
    background: '#fff'
  },
  css: [
    { src: '@/assets/css/style.less', lang: 'less' },
    { src: '@/assets/css/main.scss', lang: 'scss' }
  ],
  plugins: [
    { src: '@/plugins/antd-ui' },
    { src: '~plugins/init-app' },
    { src: '~plugins/filter' },
    { src: '~plugins/use', mode: 'client' },
    { src: '~plugins/ag-grid', mode: 'client' }
  ],
  buildModules: [
    '@nuxtjs/eslint-module'
  ],
  modules: [
    ['vue-scrollto/nuxt'],
    // 'nuxt-svg-loader',
    '@nuxtjs/svg'
  ],
  server: {
    port: 4000, // default: 3000
    host: '0.0.0.0'
  },
  build: {
    babelrc: true,
    extend (config, ctx) {
    // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)|(\.svg$)/,
          options: {
            fix: true
          }
        })
      }
    },
    transpile: [/^vue2-google-maps($|\/)/],
    loaders: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          'link-color': '#ff7875', // red-4
          'primary-color': '#ff7875' // red-4
        }
      }
    },
    terser: {
      terserOptions: {
        compress: {
          drop_console: false
        }
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '.',
        maxAsyncRequests: 7,
        cacheGroups: {
          lib: {
            test: /[\\/]node_modules[\\/](@aws-amplify|aws-sdk|aws-amplify|aws-appsync)[\\/]/,
            chunks: 'all',
            priority: 20,
            name: true
          },
          ui: {
            test: /[\\/]node_modules[\\/](@ant-design|ant-design-vue)[\\/]/,
            chunks: 'all',
            priority: 20,
            name: true
          },
          utility: {
            test: /[\\/]node_modules[\\/](moment|lodash)[\\/]/,
            chunks: 'all',
            priority: 20,
            name: true
          }
        }
      }
    }
  }
}
