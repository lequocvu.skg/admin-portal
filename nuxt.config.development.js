require('dotenv').config({ path: '.env.development' })
const config = require('./nuxt.config.js')
const nuxtConfig = { ...config }

module.exports = nuxtConfig
