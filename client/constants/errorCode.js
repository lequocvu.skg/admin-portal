const errorCodeMapping = {
  LOGIN_FAILED: 'Đăng nhập thất bại, vui lòng kiểm nhập đúng tài khoản và mật khẩu.',
  account_inactive: 'Tài khoản này đã bị vô hiệu hoá.'
}

export default errorCodeMapping
