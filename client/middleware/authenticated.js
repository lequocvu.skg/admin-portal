export default function ({ store, redirect, route, app }) {
  const isAuthenticated = store.getters['auth/isAuthenticated']
  if (!isAuthenticated) {
    return redirect({ path: '/login' })
  }
}
