/**
 * window.resize
 */
export function triggerWindowResizeEvent () {
  const event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

export const normalizeKey = key => decodeURIComponent(key.replace(/\+/g, ' '))
export const validateJapanPhoneNumber = (number) => {
  const rgex = /^[0][1-9]\d{9}$|^[1-9]\d{9}$/g
  return number && !rgex.test(number).toLowerCase()
}
export const standardizedParmas = (input) => {
  return Object.entries(input).reduce(
    (a, [k, v]) => (v ? { ...a, [k]: v } : a),
    {}
  )
}

export const modifyTextForLinks = (textToCheck, className) => {
  // URLs starting with http://, https://, or ftp://
  const replacePattern1 = /(https?:\/\/[^\s]+)/gi

  // URLs starting with www. (without // before it, or it'd re-link the ones done above)
  const replacePattern2 = /(^|[^/])(www\.[\S]+(\b|$))/gi

  // Change email addresses to mailto:: links
  const replacePattern3 = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi

  return textToCheck
    .replace(replacePattern1, `<a class="${className}" href="$1" target="_blank">$1</a>`)
    .replace(replacePattern2, `$1<a class="${className}" href="http://$2" target="_blank">$2</a>`)
    .replace(replacePattern3, `<a class="${className}" href="mailto:$1">$1</a>`)
}
