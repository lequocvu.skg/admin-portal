import Search from './Search.vue'
import ChangeSupervisorDialog from './ChangeSupervisorDialog.vue'

export {
  Search,
  ChangeSupervisorDialog
}
