import axios from 'axios'
import message from 'ant-design-vue/es/message'
import Vue from 'vue'
const service = axios.create({
  baseURL: process.env.NUXT_ENV_BASE_URL,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json'
  }
})

const handleError = (error) => {
  if (!error.response) {
    message.error('Lỗi mạng')
    return Promise.reject(new Error('Lỗi mạng'))
  } else {
    const data = error.response.data
    if (error.response.status === 403) {
      message.error(data.message)
    }

    if (error.response.status === 401) {
      message.error('Xác thưc phân quyền thất bại')
    }
    return Promise.reject(error.response.data.error)
  }
}

service.interceptors.request.use((config) => {
  const token = Vue.ls.get('Access-Token')
  if (token) {
    config.headers.Authorization = token
  }
  return config
}, handleError)

service.interceptors.response.use((response) => {
  if (response.status === 204) {
    return {}
  }
  const { data } = response
  if (data.status === 'OK') {
    if (Array.isArray(data.data)) {
      return data
    }
    return data.data
  }

  if (data.status === 'NG') {
    throw data.error
  }
  message.error('Hệ thống xảy ra lỗi, vui lòng thử lại.')
  throw new Error('Hệ thống xảy ra lỗi.')
}, handleError)

export default service
