import service from './request'

class EHRAdminApi {
  constructor () {
    this.user = {
      login: ({ username, password }) => {
        return this.call('/Accounts/login', {
          method: 'post',
          body: { username, password }
        })
      },
      getAccount: (id) => {
        return this.call(`/Accounts/${id}`, {
          method: 'get'
        })
      },
      logout: () => {
        return this.call('/Accounts/logout', {
          method: 'post'
        })
      },
      me: () => {
        return this.call('/Accounts/me', {
          method: 'get'
        })
      },
      resetPassword: ({ email }) => {
        return this.call('/Accounts/reset', {
          method: 'post',
          body: {
            email
          }
        })
      },
      changePassword: async ({ newPassword }, resetPasswordToken) => {
        const data = { newPassword }
        const response = await service.post('/Accounts/reset-password', data, {
          headers: {
            Authorization: resetPasswordToken
          }
        })
        return response
      }
    }
    this.consultant = {
      getPatients: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/consultantPatients?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      }
    }
    this.supervisor = {
      getSupporters: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/supervisorSupporters?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      createSupporter: (params) => {
        return this.call('/Accounts/supporter', {
          method: 'post',
          body: params
        })
      },
      getPatients: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/supervisorPatients?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      updateSupporter: (payload) => {
        return this.call('/Accounts/supporter', {
          method: 'patch',
          body: payload
        })
      },
      updateSupporterStt: ({ id, status }) => {
        return this.call('/Accounts/supporter', {
          method: 'patch',
          body: {
            id, status
          }
        })
      }
    }
    this.admin = {
      getPatients: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/adminPatients?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      getSupporters: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/adminSupporters?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      getSupervisors: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/adminSupervisors?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      getConsultants: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/adminConsultants?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      createConsultant: (params) => {
        return this.call('/Accounts/createConsultant', {
          method: 'post',
          body: params
        })
      },
      createSupervisor: (params) => {
        return this.call('/Accounts/createSupervisor', {
          method: 'post',
          body: params
        })
      },
      updateSupervisor: (payload) => {
        return this.call('/Accounts/supervisor', {
          method: 'patch',
          body: payload
        })
      },
      updateConsultant: (payload) => {
        return this.call('/Accounts/consultant', {
          method: 'patch',
          body: payload
        })
      },
      getSupervisorById: (id) => {
        return this.call(`/Accounts/${id}`, {
          method: 'get'
        })
      },
      getConsultantById: (id) => {
        return this.call(`/Accounts/${id}`, {
          method: 'get'
        })
      },
      updateConsultantStt: ({ id, status }) => {
        return this.call('/Accounts/consultant', {
          method: 'patch',
          body: {
            id,
            status
          }
        })
      },
      updateSupervisorStt: ({ id, status }) => {
        return this.call('/Accounts/supervisor', {
          method: 'patch',
          body: {
            id,
            status
          }
        })
      },
      updatePatientStt: ({ id, status }) => {
        return this.call('/Accounts/patient', {
          method: 'patch',
          body: {
            id,
            status
          }
        })
      },
      createSupporter: (params) => {
        return this.call('/Accounts/supporter', {
          method: 'post',
          body: params
        })
      },
      updateSupporter: (payload) => {
        return this.call('/Accounts/supporter', {
          method: 'patch',
          body: payload
        })
      },
      updateSupporterStt: ({ id, status }) => {
        return this.call('/Accounts/supporter', {
          method: 'patch',
          body: {
            id, status
          }
        })
      }
    }
    this.supporter = {
      getPatients: ({ filter, page = 1 }) => {
        return this.call(`/Accounts/supporterPatients?filter=${JSON.stringify(filter)}&page=${page}`, {
          method: 'get'
        })
      },
      createPatient: (params) => {
        return this.call('/Accounts/create-patient', {
          method: 'post',
          body: params
        })
      },
      getInstantCode: ({ fhirId }) => {
        return this.call('/Accounts/instant-code', {
          method: 'post',
          body: {
            fhir_id: fhirId
          }
        })
      },
      updatePatientStt: ({ id, status }) => {
        return this.call('/Accounts/patient', {
          method: 'patch',
          body: {
            id,
            status
          }
        })
      },
      updatePatient: (payload) => {
        return this.call('/Accounts/patient', {
          method: 'patch',
          body: payload
        })
      },
      getPatientById: (id) => {
        return this.call(`/Accounts/${id}`, {
          method: 'get'
        })
      }
    }
    this.validate = {
      username: ({ username }) => {
        return this.call(`/Accounts/has-created?username=${username}`, {
          method: 'get'
        })
      },
      email: ({ email }) => {
        return this.call(`/Accounts/has-created?email=${email}`, {
          method: 'get'
        })
      }
    }
  }

  async call (url, { method, routeParams = {}, urlParams = {}, body = {} }) {
    Object.keys(routeParams).forEach((key) => {
      url = url.replace(
        new RegExp(':' + key + '(/|$)', 'g'),
        routeParams[key] + '$1'
      )
    })
    const response = await service.request({
      method,
      params: urlParams,
      url: `${url}`,
      data: body ? JSON.stringify(body) : undefined
    })
    return response
  }
}

const ehrApi = new EHRAdminApi()

export default ehrApi
