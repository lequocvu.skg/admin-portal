import RouteView from '~/layouts/RouteView'
import consultantIcon from '~/assets/icons/consultant.svg?inline'
import supervisorIcon from '~/assets/icons/supervisor.svg?inline'
import supporterIcon from '~/assets/icons/supporter.svg?inline'
import patientIcon from '~/assets/icons/patient.svg?inline'

const interopDefault = (promise) => {
  return promise.then(m => m.default || m)
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'home',
    meta: { title: 'Home' },
    redirect: '/account',
    component: () => interopDefault(import('~/dynamic_pages/index')),
    children: [
      {
        path: '/supervisors',
        name: 'root-supervisor',
        redirect: '/supervisors',
        hideChildrenInMenu: true,
        component: RouteView,
        meta: {
          title: 'Quản lý Giám sát viên',
          icon: supervisorIcon,
          keepAlive: false,
          groups: ['admin']
        },
        children: [
          {
            path: '/supervisors',
            name: 'list-supervisor',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supervisor/index')),
            meta: {
              title: 'List account',
              icon: 'user-add',
              groups: ['admin']
            }
          },
          {
            path: '/supervisors/create',
            name: 'create-supervisor',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supervisor/create')),
            meta: {
              title: 'Create supervisor',
              icon: 'user-add',
              groups: ['admin']
            }
          },
          {
            path: '/supervisors/:id',
            name: 'update-supervisor',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supervisor/id')),
            meta: {
              title: 'Update supervisor',
              icon: 'edit',
              groups: ['admin']
            }
          }
        ]
      },
      {
        path: '/consultants',
        name: 'root-consultants',
        redirect: '/consultants',
        hideChildrenInMenu: true,
        component: RouteView,
        meta: {
          title: 'Quản lý Tư vấn viên',
          icon: consultantIcon,
          keepAlive: false,
          groups: ['admin']
        },
        children: [
          {
            path: '/consultants',
            name: 'list-consultant',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/consultant/index')),
            meta: {
              title: 'List consultants',
              icon: 'user-add',
              groups: ['admin']
            }
          },
          {
            path: '/consultants/create',
            name: 'create-consultant',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/consultant/create')),
            meta: {
              title: 'Create consultant',
              icon: 'user-add',
              groups: ['admin']
            }
          },
          {
            path: '/consultants/:id',
            name: 'update-consultant',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/consultant/id')),
            meta: {
              title: 'Update consultant',
              icon: 'edit',
              groups: ['admin']
            }
          }
        ]
      },
      {
        path: '/supporters',
        name: 'root-supporter',
        redirect: '/supporters',
        hideChildrenInMenu: true,
        component: RouteView,
        meta: {
          title: 'Quản lý Nhân viên CSKH',
          icon: supporterIcon,
          keepAlive: false,
          groups: ['admin', 'supervisor']
        },
        children: [
          {
            path: '/supporters',
            name: 'list-supporter',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supporter/index')),
            meta: {
              title: 'List supporter',
              icon: 'user-add',
              groups: ['admin', 'supervisor']
            }
          },
          {
            path: '/supporters/create',
            name: 'create-supporter',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supporter/create')),
            meta: {
              title: 'Create supporter',
              icon: 'user-add',
              groups: ['admin', 'supervisor']
            }
          },
          {
            path: '/supporters/:id',
            name: 'update-supporter',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/supporter/id')),
            meta: {
              title: 'Update supporter',
              icon: 'edit',
              groups: ['admin', 'supervisor']
            }
          }
        ]
      },
      {
        path: '/account',
        name: 'root-account',
        redirect: '/account',
        hideChildrenInMenu: true,
        component: RouteView,
        meta: {
          title: 'Quản lý Khách hàng',
          icon: patientIcon,
          keepAlive: false,
          groups: ['supporter', 'admin', 'supervisor', 'consultant']
        },
        children: [
          {
            path: '/account',
            name: 'list-account',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/account/index')),
            meta: {
              title: 'List account',
              icon: 'user-add',
              groups: ['supporter', 'admin', 'supervisor', 'consultant']
            }
          },
          {
            path: '/account/create',
            name: 'create-account',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/account/create')),
            meta: {
              title: 'Create account',
              icon: 'user-add',
              groups: ['supporter', 'admin', 'supervisor', 'consultant']
            }
          },
          {
            path: '/account/:id',
            name: 'update-account',
            hidden: true,
            component: () =>
              interopDefault(import('@/dynamic_pages/account/id')),
            meta: {
              title: 'Updatea account',
              icon: 'edit',
              groups: ['supporter', 'admin', 'supervisor', 'consultant']
            }
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/exception/403',
    hidden: true
  }
]

export const constantRouterMap = []
