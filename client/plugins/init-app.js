import { ehrApi } from '@/api'
// import { asyncRouterMap } from '../config/router.config'
export default async function ({ store, app }) {
  // app.router.addRoutes(asyncRouterMap)
  try {
    const user = await ehrApi.user.me()
    if (user) {
      const roles = user.roles
      const groups = [roles[0].name]
      await store.dispatch('permission/generateAsyncRoutes', {
        groups
      })
      app.router.addRoutes(store.getters['permission/addRouters'])
      await store.dispatch('auth/saveInfo', user)
    }
    app.router.beforeEach((to, from, next) => {
      const routes = ['/exception', '/login', '/reset-password']
      if (routes.includes(to.path)) {
        return next()
      }
      next()
    })
  } catch (error) {
    console.log(error.message)
  }
}
