import Vue from 'vue'
import moment from 'moment'
const userStatus = {
  ACTIVE: 'Đang hoạt động',
  INACTIVE: 'Bị vô hiệu'
}

const userRole = {
  admin: 'Quản trị viên',
  supporter: 'Nhân viên CSKH',
  patient: 'Khách hàng',
  supervisor: 'Giám sát viên',
  consultant: 'Tư vấn viên'
}

const namePrefix = {
  male: 'Mr.',
  female: 'Ms.'
}

const gender = {
  male: 'Nam',
  female: 'Nữ'
}

Vue.filter('FromNow', function (dataStr, suffix = false) {
  if (!dataStr) {
    return ''
  }
  return moment(dataStr).fromNow(suffix)
})

Vue.filter('DateFormat', function (dataStr, pattern = 'YYYY/MM/DD HH:mm:ss') {
  if (!dataStr) {
    return ''
  }
  return moment(dataStr).format(pattern)
})

Vue.filter('UserStatus', function (stt) {
  if (!stt) { return '' }
  return userStatus[stt] || ''
})

Vue.filter('NamePrefix', function (gender) {
  if (!gender) { return '' }
  return namePrefix[gender] || ''
})

Vue.filter('UserRole', function (role) {
  if (!role) { return '' }
  return userRole[role] || ''
})

Vue.filter('Gender', function (g) {
  if (!g) { return '' }
  return gender[g] || ''
})

Vue.filter('ImageURL', function (url) {
  if (!url) { return '/images/noimage.svg' }
  const index = url.lastIndexOf('.')
  const imgExtend = url.substr(index + 1)
  const mustBeImage = ['jpg', 'jpeg', 'png', 'svg', 'webp', 'heic'].includes(imgExtend.toLowerCase())
  const mustInS3 = [process.env.NUXT_ENV_RECRUITER_BUCKET, process.env.NUXT_ENV_SEEKER_BUCKET].some(path => url.includes(path))
  if (!mustBeImage || !mustInS3) { return '/images/noimage.svg' }
  return url
})
