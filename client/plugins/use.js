import Vue from 'vue'
import VueRx from 'vue-rx'
import VueStorage from 'vue-ls'
import { SGrid } from '@/components/grid'
import Ellipsis from '@/components/Ellipsis/Ellipsis'
import VueChatScroll from 'vue-chat-scroll'
import VueLazyload from 'vue-lazyload'

Vue.use(VueRx)
Vue.use(VueStorage, {
  namespace: 'EHR_', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
})

// application bus
const eventBus = {}

eventBus.install = function (Vue) {
  Vue.prototype.$bus = new Vue()
}

Vue.use(eventBus)

// global components
Vue.component('SGrid', SGrid)
Vue.component('Ellipsis', Ellipsis)

// Maps
Vue.use(VueChatScroll)

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/noimage.svg',
  loading: '/images/loading.svg',
  filter: {
    imagePath (el) {
      if (!el.src) {
        el.src = ''
      } else {
        const index = el.src.lastIndexOf('.')
        const imgExtend = el.src.substr(index + 1)
        const mustBeImage = ['jpg', 'jpeg', 'png', 'svg', 'webp', 'heic'].includes(imgExtend.toLowerCase())
        const mustInS3 = [process.env.NUXT_ENV_RECRUITER_BUCKET, process.env.NUXT_ENV_SEEKER_BUCKET].some(path => el.src.includes(path))
        if (!mustBeImage || !mustInS3) { el.src = '' }
      }
    }
  },
  lazyComponent: true
})
