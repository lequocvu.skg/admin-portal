import UserDetail from './UserDetail'
import CreateUser from './CreateUser'

export {
  UserDetail,
  CreateUser
}
