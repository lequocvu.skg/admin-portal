export const parseUserItem = (item) => {
  const { Attributes: attributes } = item
  const att = []
  attributes.forEach((item) => {
    att[item.Name] = item.Value
  })
  return {
    ...att,
    username: item.Username,
    enabled: item.Enabled,
    userStatus: item.UserStatus,
    created: item.UserCreateDate,
    updated: item.UserLastModifiedDate,
    groups: item.groups || []
  }
}
