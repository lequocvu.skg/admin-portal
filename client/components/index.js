import Ellipsis from '@/components/Ellipsis'
import DescriptionList from '@/components/DescriptionList'
import MultiTab from '@/components/MultiTab'
import Result from '@/components/Result'
import TagSelect from '@/components/TagSelect'
import ExceptionPage from '@/components/Exception'

import { Authenticator } from '@/components/Authenticator'

export {
  Ellipsis,
  DescriptionList as DetailList,
  MultiTab,
  Result,
  ExceptionPage,
  TagSelect,

  Authenticator
}
