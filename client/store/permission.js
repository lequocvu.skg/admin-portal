import Vue from 'vue'
import { asyncRouterMap, constantRouterMap } from '@/config/router.config'

function hasPermission (groups, route) {
  if (route.meta && route.meta.groups) {
    let flag = false
    for (let i = 0, len = groups.length; i < len; i++) {
      flag = route.meta.groups.includes(groups[i])
      if (flag) {
        return true
      }
    }
    return false
  }
  return true
}

function filterAsyncRouter (routerMap, groups) {
  const accessedRouters = routerMap.filter((route) => {
    if (hasPermission(groups, route)) {
      if (route.children && route.children.length) {
        route.children = filterAsyncRouter(route.children, groups)
      }
      return true
    }
    return false
  })
  return accessedRouters
}

const initialState = () => {
  return {
    routers: constantRouterMap,
    addRouters: asyncRouterMap
  }
}

// initial state
export const state = () => initialState()

// getters
export const getters = {
  addRouters: state => state.addRouters,
  routers: state => state.routers
}

// actions
export const actions = {
  generateAsyncRoutes ({ commit }, data = {}) {
    return new Promise((resolve) => {
      const { groups = [] } = data
      const accessedRouters = filterAsyncRouter(asyncRouterMap, groups)
      commit('SET_ROUTERS', accessedRouters)
      resolve()
    })
  }
}

// mutations
export const mutations = {
  SET_ROUTERS: (state, routers) => {
    Vue.set(state, 'addRouters', routers)
    Vue.set(state, 'routers', constantRouterMap.concat(routers))
  }
}
