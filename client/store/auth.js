import { ehrApi } from '@/api'
import Vue from 'vue'
const initialState = () => {
  return {
    userInfo: {},
    token: null,
    role: ''
  }
}

// initial state
export const state = () => initialState()

// getters
export const getters = {
  userInfo: state => state.userInfo,
  isAuthenticated: state => !!state.userInfo.id,
  name: state => state.userInfo && state.userInfo.name,
  avatar: state => state.avatar,
  userId: state => state.userInfo && state.userInfo.id,
  role: state => state.role,
  isAdmin: state => state.role === 'admin',
  isSupervisor: state => state.role === 'supervisor',
  isManager: state => state.role === 'admin' || state.role === 'supervisor',
  isSupporter: state => state.role === 'supporter',
  isConsultant: state => state.role === 'consultant'
}

// actions
export const actions = {
  saveInfo ({ commit }, userInfo) {
    commit('SET_USER', userInfo)
  },
  async login ({ commit }, credentials) {
    const result = await ehrApi.user.login(credentials)
    const { id, ttl } = result
    const expire = ttl * 1000
    Vue.ls.set('Access-Token', id, expire)
    commit('SET_TOKEN', id)
  },
  async logout ({ commit }) {
    await ehrApi.user.logout()
    commit('SET_TOKEN', '')
    Vue.ls.remove('Access-Token')
  }
}

// mutations
export const mutations = {
  SET_USER: (state, user) => {
    const modifyUser = { ...user }
    const roles = modifyUser.roles
    roles.length > 0 && Vue.set(state, 'role', roles[0].name)
    Vue.set(state, 'userInfo', modifyUser)
  },
  RESET (state) {
    Object.assign(state, initialState())
  },
  SET_TOKEN: (state, token) => {
    Vue.set(state, 'token', token)
  }
}
