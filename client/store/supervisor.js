import Vue from 'vue'
import ehrApi from '../api/ehr'
const initialState = () => {
  return {
    entities: {},
    ids: [],
    loading: false,
    page: 1,
    total: 1
  }
}

// initial state
export const state = () => initialState()

// getters
export const getters = {
  ids: state => state.ids,
  supervisors: (state, getters) => {
    return getters.ids.map(id => state.entities[id]) || []
  },
  page: state => state.page,
  supervisorById: state => id => state.entities[id],
  loading: state => state.loading,
  total: state => state.total
}

// actions
export const actions = {
  async getSupervisors ({ dispatch }, params) {
    try {
      await dispatch('reset')
      dispatch('setLoading', true)
      const { filter, page: queryPage } = params
      const res = await ehrApi.admin.getSupervisors({ filter, page: queryPage })
      const { data: items, currentPage: page, totalItemCount: total } = res
      dispatch('loadSupervisors', { payload: items, page, total })
    } catch (error) {
      // console.error(error)
    } finally {
      dispatch('setLoading', false)
    }
  },
  addSupervisor ({ dispatch }, supervisor) {
    try {
      dispatch('loadSupervisors', { payload: supervisor })
    } catch (error) {
      console.error(error)
    }
  },
  updateSupervisor ({ commit }, payload) {
    return new Promise((resolve) => {
      commit('UPDATE', payload)
      resolve()
    })
  },
  setLoading ({ commit }, value) {
    return new Promise((resolve) => {
      commit('SET_LOADING', value)
      resolve()
    })
  },
  loadSupervisors ({ commit }, payload) {
    return new Promise((resolve) => {
      commit('SET', payload)
      resolve()
    })
  },
  reset ({ commit }) {
    return new Promise((resolve) => {
      commit('RESET')
      resolve()
    })
  }
}

// mutations
export const mutations = {
  SET (state, { payload, page, total }) {
    if (Array.isArray(payload)) {
      let newIds = [...state.ids]
      const newEntities = Object.assign({}, state.entities)

      payload.forEach((values) => {
        const id = values.id
        newIds = [...newIds, id]
        newEntities[id] = Object.assign({}, newEntities[id], values)
      })

      const ids = Array.from(new Set(newIds))
      const entities = Object.assign({}, newEntities)

      Vue.set(state, 'ids', ids)
      Vue.set(state, 'page', page)
      Vue.set(state, 'total', total)
      Vue.set(state, 'entities', Object.assign({}, entities))
    } else {
      console.log(payload)
      const id = payload.id
      const newIds = Array.from(new Set([id, ...state.ids]))
      const newEntities = Object.assign({}, state.entities, {
        [id]: Object.assign({}, state.entities[id], payload)
      })
      Vue.set(state, 'ids', newIds)
      Vue.set(state, 'entities', newEntities)
    }
  },
  SET_LOADING (state, value) {
    Vue.set(state, 'loading', value)
  },
  REMOVE (state, payload) {
    const newEntities = Object.assign({}, state.entities)
    let newIds = [...state.ids]

    if (payload.id in newEntities) {
      delete newEntities[payload.id]
    }
    newIds = newIds.filter(id => id in newEntities)
    Vue.set(state, 'ids', newIds)
    Vue.set(state, 'entities', newEntities)
  },
  UPDATE (state, payload) {
    const newEntities = Object.assign({}, state.entities)
    let newIds = [...state.ids]

    if (payload.id in newEntities) {
      newEntities[payload.id] = Object.assign(
        {},
        newEntities[payload.id],
        payload
      )
    }
    newIds = newIds.filter(id => id in newEntities)
    Vue.set(state, 'ids', newIds)
    Vue.set(state, 'entities', newEntities)
  },
  RESET (state) {
    Object.assign(state, initialState())
  }
}
